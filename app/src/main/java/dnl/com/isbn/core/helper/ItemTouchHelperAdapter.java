package dnl.com.isbn.core.helper;

/**
 * Created by Dev.TuanNguyen on 7/11/2017.
 */

public interface ItemTouchHelperAdapter {
    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}

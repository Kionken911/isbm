package dnl.com.isbn;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dnl.com.isbn.core.commons.Constants;
import dnl.com.isbn.core.model.ISBN;

public class BookDetail extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout btnBack;
    ImageView imgThumbnail;
    TextView btnShare;
    TextView tvTitle, tvAuthor, tvISBN, tvPublishedDate, tvPublisher, tvPageCount, tvCategory, tvLanguage, tvCountry, tvDescription;
    private ISBN isbn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        addControl();
        addEvent();


    }

    private void addEvent() {
        getDatail();
        btnBack.setOnClickListener(this);
    }

    private void getDatail() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isbn = getIntent().getParcelableExtra(Constants.PARAM_INTENT_DATA);
        }
        if (isbn != null) {
            showDetail();
        }
    }

    private void showDetail() {
        Picasso.with(BookDetail.this).load(isbn.getThumbnail()).into(imgThumbnail);
        tvTitle.setText(isbn.getTitle());
        if (isbn.getAuthors() != null) {
            tvAuthor.setText(isbn.getAuthors().get(0));
        }

        tvISBN.setText("ISBN: " + isbn.getISBN_13());
        tvPublishedDate.setText("Published Date: " + isbn.getPublishedDate());
        tvPublisher.setText("Publisher: " + isbn.getPublisher());
        tvPageCount.setText("Page Count: " + isbn.getPageCount());
        if (isbn.getCategories() != null) {
            tvCategory.setText("Categories: " + isbn.getCategories().get(0));
        }
        tvLanguage.setText("Language: " + isbn.getLanguage());
        tvCountry.setText("Country: " + isbn.getCountry());
        tvDescription.setText(isbn.getDescription());
    }

    private void addControl() {
        btnBack = (RelativeLayout) findViewById(R.id.btnBack);
        btnShare = (TextView) findViewById(R.id.btnShare);

        imgThumbnail = (ImageView) findViewById(R.id.imgThumbnail);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvAuthor = (TextView) findViewById(R.id.tvAuthor);
        tvISBN = (TextView) findViewById(R.id.tvISBN);
        tvPublishedDate = (TextView) findViewById(R.id.tvPublishedDate);
        tvPublisher = (TextView) findViewById(R.id.tvPublisher);
        tvPageCount = (TextView) findViewById(R.id.tvPageCount);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvLanguage = (TextView) findViewById(R.id.tvLanguage);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack: {
                finish();
            }
        }
    }
}

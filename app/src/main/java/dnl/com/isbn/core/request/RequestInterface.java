package dnl.com.isbn.core.request;

/**
 * Created by Dev.TuanNguyen on 7/11/2017.
 */

public interface RequestInterface {
    void sendGetRequest(String code,RequestListener listener);
}

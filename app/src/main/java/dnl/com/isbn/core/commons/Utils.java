package dnl.com.isbn.core.commons;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Dev.TuanNguyen on 7/19/2017.
 */

public class Utils {

    public static File createFile(String path, byte[] content, boolean overwrite) throws IOException {
        if (overwrite) {
            return writeToSDFile(content, path);
        } else {
            File curFile = new File(path);
            if (curFile.exists()) {
                throw new IOException("File exist");
            } else {
                return writeToSDFile(content, path);
            }
        }
    }

    public static File writeToSDFile(byte[] bytesFileContent, String path) throws IOException {
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard.getAbsolutePath() + path);
        directory.mkdirs();
        File file = new File(directory, "mysdfile.txt");
        FileOutputStream fOut = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fOut);
        osw.write(new String(bytesFileContent));
        osw.flush();
        osw.close();
        return file;
    }
}

package dnl.com.isbn.core.commons;

/**
 * Created by Dev.TuanNguyen on 7/17/2017.
 */

public class Constants {
    public static final String PARAM_ITEMS = "items";
    public static final String PARAM_ID = "id";
    public static final String PARAM_VOLUME_INFO = "volumeInfo";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_AUTHORS = "authors";
    public static final String PARAM_PUBLISHER = "publisher";
    public static final String PARAM_PUBLISHED_DATE = "publishedDate";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_INDUSTRY_IDENTIFIERS = "industryIdentifiers";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_IDENTIFIER = "identifier";
    public static final String ISBN_10 = "ISBN_10";
    public static final String ISBN_13 = "ISBN_13";
    public static final String PARAM_CATEGORIES = "categories";
    public static final String PARAM_IMAGE_LINKS = "imageLinks";
    public static final String PARAM_SMALL_THUMBNAIL = "smallThumbnail";
    public static final String PARAM_THUMBNAIL = "thumbnail";
    public static final String PARAM_LANGUAGE = "language";
    public static final String PARAM_ACCESS_INFO = "accessInfo";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_SEARCH_INFO = "searchInfo";
    public static final String PARAM_TEXT_SNIPPET = "textSnippet";
    public static final String PARAM_INTENT_DATA = "PARAM_INTENT_DATA";
    public static final String PARAM_PAGE_COUNT = "pageCount";
}

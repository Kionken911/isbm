package dnl.com.isbn.core.request;

/**
 * Created by Dev.TuanNguyen on 7/11/2017.
 */

public interface RequestListener {
    void onSuccess(String response);

    void onFail(String s);
}

package dnl.com.isbn.core.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dnl.com.isbn.R;
import dnl.com.isbn.core.model.ISBN;

/**
 * Created by Dev.TuanNguyen on 7/12/2017.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ISBNHolder> {

    private List<ISBN> isbnList;
    private LayoutInflater layoutInflater;
    private Context context;

    private ItemClickCallback itemClickCallback;

    public interface ItemClickCallback {
        void onItemClick(int position);
    }

    public void setItemClickCallback(ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }

    public BookAdapter(List<ISBN> isbnData, Context context) {
        this.isbnList = isbnData;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ISBNHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new ISBNHolder(view);
    }

    @Override
    public void onBindViewHolder(ISBNHolder holder, int position) {
        ISBN isbn = isbnList.get(position);
        holder.tvTitle.setText(isbn.getTitle());
        if(isbn.getAuthors() != null && isbn.getAuthors().size() > 0){
            holder.tvAuthor.setText(isbn.getAuthors().get(0));
        }else {
            holder.tvAuthor.setText("");
        }
        holder.tvDescription.setText(isbn.getDescription());
        Picasso.with(context).load(isbn.getSmallThumbnail()).into(holder.imgThumbnail);
    }

    @Override
    public int getItemCount() {
        return isbnList.size();
    }

    class ISBNHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle, tvAuthor, tvDescription;
        private ImageView imgThumbnail;
        private View contain;

        public ISBNHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvAuthor = (TextView) itemView.findViewById(R.id.tvAuthor);
            imgThumbnail = (ImageView) itemView.findViewById(R.id.imgThumbnail);
            contain = itemView.findViewById(R.id.cont_item_root);

            contain.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cont_item_root: {
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                }
            }
        }
    }
}

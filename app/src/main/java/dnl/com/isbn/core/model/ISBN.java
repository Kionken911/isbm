package dnl.com.isbn.core.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dnl.com.isbn.core.commons.Constants;

import static dnl.com.isbn.core.commons.Constants.PARAM_ACCESS_INFO;
import static dnl.com.isbn.core.commons.Constants.PARAM_AUTHORS;
import static dnl.com.isbn.core.commons.Constants.PARAM_CATEGORIES;
import static dnl.com.isbn.core.commons.Constants.PARAM_COUNTRY;
import static dnl.com.isbn.core.commons.Constants.PARAM_DESCRIPTION;
import static dnl.com.isbn.core.commons.Constants.PARAM_ID;
import static dnl.com.isbn.core.commons.Constants.PARAM_IDENTIFIER;
import static dnl.com.isbn.core.commons.Constants.PARAM_IMAGE_LINKS;
import static dnl.com.isbn.core.commons.Constants.PARAM_INDUSTRY_IDENTIFIERS;
import static dnl.com.isbn.core.commons.Constants.PARAM_ITEMS;
import static dnl.com.isbn.core.commons.Constants.PARAM_LANGUAGE;
import static dnl.com.isbn.core.commons.Constants.PARAM_PAGE_COUNT;
import static dnl.com.isbn.core.commons.Constants.PARAM_PUBLISHED_DATE;
import static dnl.com.isbn.core.commons.Constants.PARAM_PUBLISHER;
import static dnl.com.isbn.core.commons.Constants.PARAM_SMALL_THUMBNAIL;
import static dnl.com.isbn.core.commons.Constants.PARAM_THUMBNAIL;
import static dnl.com.isbn.core.commons.Constants.PARAM_TITLE;
import static dnl.com.isbn.core.commons.Constants.PARAM_TYPE;
import static dnl.com.isbn.core.commons.Constants.PARAM_VOLUME_INFO;

/**
 * Created by Dev.TuanNguyen on 7/11/2017.
 */

@SuppressLint("ParcelCreator")
public class ISBN implements Parcelable {

    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("authors")
    private List<String> authors;
    @Expose
    @SerializedName("publisher")
    private String publisher;
    @Expose
    @SerializedName("publishedDate")
    private String publishedDate;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("ISBN_10")
    private String ISBN_10;
    @Expose
    @SerializedName("ISBN_13")
    private String ISBN_13;
    @Expose
    @SerializedName("pageCount")
    private int pageCount;
    @Expose
    @SerializedName("categories")
    private List<String> categories;
    @Expose
    @SerializedName("thumbnail")
    private String thumbnail;
    @Expose
    @SerializedName("smallThumbnail")
    private String smallThumbnail;
    @Expose
    @SerializedName("language")
    private String language;
    @Expose
    @SerializedName("country")
    private String country;


    public ISBN() {
    }

    public ISBN(String id, String title, List<String> authors, String publisher, String publishedDate, String description, String ISBN_10, String ISBN_13, int pageCount, List<String> categories, String thumbnail, String smallThumbnail, String language, String country, String textSnippet) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.publishedDate = publishedDate;
        this.description = description;
        this.ISBN_10 = ISBN_10;
        this.ISBN_13 = ISBN_13;
        this.pageCount = pageCount;
        this.categories = categories;
        this.thumbnail = thumbnail;
        this.smallThumbnail = smallThumbnail;
        this.language = language;
        this.country = country;
    }

    public ISBN(String response) throws JSONException {
        JSONObject responseObject = new JSONObject(response);
        if (responseObject != null) {
            if (responseObject.has(PARAM_ITEMS)) {
                JSONArray itemArray = responseObject.optJSONArray(PARAM_ITEMS);
                for (int i = 0; i < 1; i++) {
                    JSONObject itemObject = itemArray.getJSONObject(i);

                    if (itemObject.has(Constants.PARAM_ID)) {
                        this.id = itemObject.optString(PARAM_ID);
                    }
                    //info
                    if (itemObject.has(Constants.PARAM_VOLUME_INFO)) {
                        JSONObject volumeInfoObject = itemObject.getJSONObject(PARAM_VOLUME_INFO);
                        if (volumeInfoObject.has(Constants.PARAM_TITLE)) {
                            this.title = volumeInfoObject.optString(PARAM_TITLE);
                        }

                        if (volumeInfoObject.has(Constants.PARAM_AUTHORS)) {
                            List<String> authorList = new ArrayList<>();
                            JSONArray authorArray = volumeInfoObject.getJSONArray(PARAM_AUTHORS);
                            for (int j = 0; j < authorArray.length(); j++) {
                                authorList.add(authorArray.optString(i));
                            }
                            this.authors = authorList;
                        }

                        //publisher
                        if (volumeInfoObject.has(PARAM_PUBLISHER)) {
                            this.publisher = volumeInfoObject.optString(PARAM_PUBLISHER);
                        }
                        if (volumeInfoObject.has(PARAM_PUBLISHED_DATE)) {
                            this.publishedDate = volumeInfoObject.optString(PARAM_PUBLISHED_DATE);
                        }
                        if (volumeInfoObject.has(PARAM_DESCRIPTION)) {
                            this.description = volumeInfoObject.optString(PARAM_DESCRIPTION);
                        }
                        //industryIdentifiers
                        if (volumeInfoObject.has(PARAM_INDUSTRY_IDENTIFIERS)) {
                            JSONArray industryIdentifierArray = volumeInfoObject.getJSONArray(PARAM_INDUSTRY_IDENTIFIERS);
                            for (int z = 0; z < industryIdentifierArray.length(); z++) {
                                JSONObject industryIdentifierObject = industryIdentifierArray.getJSONObject(z);
                                String type = "";
                                if (industryIdentifierObject.has(PARAM_TYPE)) {
                                    type = industryIdentifierObject.optString(PARAM_TYPE);
                                }
                                if (industryIdentifierObject.has(PARAM_IDENTIFIER)) {
                                    if (type.equals(Constants.ISBN_10)) {
                                        this.ISBN_10 = industryIdentifierObject.optString(PARAM_IDENTIFIER);
                                    } else {
                                        this.ISBN_13 = industryIdentifierObject.optString(PARAM_IDENTIFIER);
                                    }
                                }
                            }
                        }
                        if (volumeInfoObject.has(PARAM_PAGE_COUNT)) {
                            int page = volumeInfoObject.optInt(PARAM_PAGE_COUNT);
                            this.pageCount = page;
                        }

                        //categories
                        if (volumeInfoObject.has(PARAM_CATEGORIES)) {
                            List<String> categoryList = new ArrayList<>();
                            JSONArray categoryArray = volumeInfoObject.getJSONArray(PARAM_CATEGORIES);
                            for (int x = 0; x < categoryArray.length(); x++) {
                                categoryList.add(categoryArray.optString(i));
                            }
                            this.categories = categoryList;
                        }

                        //imageLinks
                        if (volumeInfoObject.has(PARAM_IMAGE_LINKS)) {
                            JSONObject imageLinkObject = volumeInfoObject.getJSONObject(PARAM_IMAGE_LINKS);
                            if (imageLinkObject.has(Constants.PARAM_SMALL_THUMBNAIL)) {
                                this.smallThumbnail = imageLinkObject.optString(PARAM_SMALL_THUMBNAIL);
                            }
                            if (imageLinkObject.has(PARAM_THUMBNAIL)) {
                                this.thumbnail = imageLinkObject.optString(PARAM_THUMBNAIL);
                            }
                        }

                        if (volumeInfoObject.has(Constants.PARAM_LANGUAGE)) {
                            this.language = volumeInfoObject.optString(PARAM_LANGUAGE);
                        }
                    }
//---
                    if (itemObject.has(PARAM_ACCESS_INFO)) {
                        JSONObject accessInfoObject = itemObject.optJSONObject(PARAM_ACCESS_INFO);
                        if (accessInfoObject.has(Constants.PARAM_COUNTRY)) {
                            this.country = accessInfoObject.optString(PARAM_COUNTRY);
                        }
                    }


                }
            }

        }
    }

    public ISBN(String title, List<String> authors, String thumbnail) {
        this.title = title;
        this.authors = authors;
        this.thumbnail = thumbnail;
    }

    public ISBN(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.authors = in.createStringArrayList();
        this.publisher = in.readString();
        this.publishedDate = in.readString();
        this.description = in.readString();
        this.ISBN_10 = in.readString();
        this.ISBN_13 = in.readString();
        this.pageCount = in.readInt();
        this.categories = in.createStringArrayList();
        this.thumbnail = in.readString();
        this.smallThumbnail = in.readString();
        this.language = in.readString();
        this.country = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getISBN_10() {
        return ISBN_10;
    }

    public void setISBN_10(String ISBN_10) {
        this.ISBN_10 = ISBN_10;
    }

    public String getISBN_13() {
        return ISBN_13;
    }

    public void setISBN_13(String ISBN_13) {
        this.ISBN_13 = ISBN_13;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSmallThumbnail() {
        return smallThumbnail;
    }

    public void setSmallThumbnail(String smallThumbnail) {
        this.smallThumbnail = smallThumbnail;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        String ISBN = "Title: " + this.title + '\n' + "Author: " + this.authors + '\n' + "description: " + this.description;
        return ISBN;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeStringList(authors);
        parcel.writeString(publisher);
        parcel.writeString(publishedDate);
        parcel.writeString(description);
        parcel.writeString(ISBN_10);
        parcel.writeString(ISBN_13);
        parcel.writeInt(pageCount);
        parcel.writeStringList(categories);
        parcel.writeString(thumbnail);
        parcel.writeString(smallThumbnail);
        parcel.writeString(language);
        parcel.writeString(country);
    }

    public static final Parcelable.Creator<ISBN> CREATOR
            = new Parcelable.Creator<ISBN>() {
        public ISBN createFromParcel(Parcel in) {
            return new ISBN(in);
        }

        public ISBN[] newArray(int size) {
            return new ISBN[size];
        }
    };


}

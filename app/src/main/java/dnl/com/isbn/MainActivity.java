package dnl.com.isbn;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sa90.materialarcmenu.ArcMenu;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dnl.com.isbn.core.adapter.BookAdapter;
import dnl.com.isbn.core.commons.Constants;
import dnl.com.isbn.core.commons.Utils;
import dnl.com.isbn.core.model.ISBN;
import dnl.com.isbn.core.request.RequestListener;
import dnl.com.isbn.core.request.RequestManager;

import static java.io.File.separator;

public class MainActivity extends AppCompatActivity implements RequestListener, BookAdapter.ItemClickCallback, View.OnClickListener {

    private RequestManager requestManager;
    private String TAG = "ISBN";
    private RecyclerView recyclerView;
    private BookAdapter bookAdapter;
    private List<ISBN> bookList;
    private List<String> isbnList;
    private ArcMenu arcMenuAndroid;
    private FloatingActionButton fabScanner;
    private String isbnCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControl();
        addEvent();
    }

    private void addEvent() {
        requestManager = new RequestManager(MainActivity.this);

        bookList = new ArrayList<>();
        isbnList = new ArrayList<>();
        bookAdapter = new BookAdapter(bookList, MainActivity.this);
        recyclerView.setAdapter(bookAdapter);
        bookAdapter.setItemClickCallback(this);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());
        itemTouchHelper.attachToRecyclerView(recyclerView);
        arcMenuAndroid.setOnClickListener(this);
        fabScanner.setOnClickListener(this);
    }


    private void addControl() {
        recyclerView = (RecyclerView) findViewById(R.id.lvISBN);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        arcMenuAndroid = (ArcMenu) findViewById(R.id.arcmenu);
        fabScanner = (FloatingActionButton) findViewById(R.id.fab_scanner);
    }


    private void scanBarcode() {
        IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                isbnCode = result.getContents();
                if (!isbnList.contains(result.getContents())) {
                    System.out.println("Account found");
                    requestManager.sendGetRequest(isbnCode, this);
                } else {
                    Toast.makeText(MainActivity.this, "ISBN existed", Toast.LENGTH_SHORT).show();
                }


//                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onSuccess(final String response) {
        Log.d(TAG, "Result:" + '\n' + response);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ISBN isbn = null;
                try {
                    isbn = new ISBN(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String path = separator + "ISBN" + separator + "data" + separator;
                try {
                    isbnList.add(isbnCode);
                    bookList.add(isbn);
                    bookAdapter.notifyDataSetChanged();
                    Utils.createFile(path, isbnList.toString().getBytes(), true);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    @Override
    public void onFail(String errors) {
        Log.d(TAG, "Result fail:" + '\n' + errors);
        Toast.makeText(this, "Result fail!" + '\n' + errors, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onItemClick(int position) {
        ISBN isbn = bookList.get(position);
        Intent intentBookDetail = new Intent(MainActivity.this, BookDetail.class);
        intentBookDetail.putExtra(Constants.PARAM_INTENT_DATA, isbn);
        //// TODO: 7/13/2017 send data to book detail
        Log.e(TAG, "ISBN: " + isbn.toString());
        startActivity(intentBookDetail);
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                deleteItem(viewHolder.getAdapterPosition());
            }
        };
        return simpleCallback;
    }

    private void deleteItem(int adapterPosition) {
        bookList.remove(adapterPosition);
        bookAdapter.notifyDataSetChanged();
    }

    private void moveItem(int viewHolderPosition, int targetPosition) {
        ISBN isbn = bookList.get(viewHolderPosition);
        bookList.remove(viewHolderPosition);
        bookList.add(targetPosition, isbn);
        bookAdapter.notifyItemMoved(viewHolderPosition, targetPosition);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.arcmenu: {
                //// TODO: 7/13/2017 Do thing?
                break;
            }
            case R.id.fab_scanner: {
                scanBarcode();
                break;
            }
        }
    }
}

package dnl.com.isbn.core.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dev.TuanNguyen on 7/10/2017.
 */

public class RequestManager implements RequestInterface {
    public static Context mContext;

    public RequestManager(Context context) {
        mContext = context;
    }

    @Override
    public void sendGetRequest(final String code, final RequestListener listener) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + code;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int totalItems = 0;
                            if (jsonObject.has("totalItems")) {
                                totalItems = jsonObject.optInt("totalItems");
                            }
                            if (totalItems > 0) {
                                listener.onSuccess(response);
                            } else {
                                listener.onFail("No find code: " + code);
                            }
                        } catch (JSONException e) {
                            listener.onFail("JSONException: " + e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
